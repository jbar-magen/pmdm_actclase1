package damp.utad.pmdm_actclase1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    long ln_tiempo_en_milisegundos_al_arrancar=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ln_tiempo_en_milisegundos_al_arrancar=System.currentTimeMillis();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("MSG_CONTROL","HE ENTRADO EN PAUSA");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("MSG_CONTROL", "HE VUELTO DE SEGUNDO PLANO");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
